<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Book_Management_System
 * @subpackage Book_Management_System/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Book_Management_System
 * @subpackage Book_Management_System/includes
 * @author     Bihanitech <ritagupta1014@gmail.com>
 */
class Book_Management_System_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
