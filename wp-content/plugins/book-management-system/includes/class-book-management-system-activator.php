<?php

/**
 * Fired during plugin activation
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Book_Management_System
 * @subpackage Book_Management_System/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Book_Management_System
 * @subpackage Book_Management_System/includes
 * @author     Bihanitech <ritagupta1014@gmail.com>
 */
class Book_Management_System_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
