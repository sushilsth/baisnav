<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
$term = get_queried_object();
?>

<?php

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
	
        <section class="bs-category-page-first-wrapper">
	
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5 col-lg-3 baishnab-category-sidebar-wrapper">
                        <div class="baishnab-category-sidebar-container">
						<div class="baishnav-category-sidebar-heading">
							<h2>Categories</h2>
										<?php

										/**
										 * Hook: woocommerce_sidebar.
										 *
										 * @hooked woocommerce_get_sidebar - 10
										 */
										//do_action( 'woocommerce_sidebar' );
										$taxonomy     = 'product_cat';
										$args = array(
											'taxonomy'     => $taxonomy,
										);
										$categories = get_categories($args);
										// var_dump(	$categories);
										//var_dump($categories);

										foreach($categories as $category) {
										
										   echo '<a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a><br>';
									
										}
										?>
										
										</div>
										
                        </div>

                    </div>
					<?php 
                    //$shoppageid = 60;
                   // if(have_rows('category_description',$shoppageid)):
                   // while( have_rows ('category_description',$shoppageid)) : the_row(); 
                ?>
					<div class="col-md-7 col-lg-9 bs-hero-category-wrapper">
                        <div class="bes-category-first-sweets-description">

					
                        <div class="bs-category-first-container">
                         <div class="bs-hero-category-hero-description">
						
							<header class="woocommerce-products-header">
								<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
									<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
								<?php// endif; ?>

								<?php
								/**
								 * Hook: woocommerce_archive_description.
								 *
								 * @hooked woocommerce_taxonomy_archive_description - 10
								 * @hooked woocommerce_product_archive_description - 10
								 */
								do_action( 'woocommerce_archive_description' );
								?>
                           </header>
                                <p><?php the_content(); ?></p>
                            </div>
                            <div class="bs-hero-category-hero-image">
							<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );?>
                          
						  <?php
							if (is_shop()){
								echo('<img src="'.$thumb['0'].'" alt="">');
							}else{
								?>
								<img src="<?php echo get_field("cat_page_hero_image", $term->taxonomy.'_'.$term->term_id); ?>" />
								<?php
								// echo do_shortcode('[categorythumbnail]');
							}
							?>
							
                            </div>
                        </div>
                    </div>
                        <!-- <div class="baishnav-category-short-by">
                            <span>Sort by</span>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Best Selling</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                       
					   
					    </div> -->
	<div class="baishnav-category-short-by">
						<span>Sort by</span>
		<form class="woocommerce-ordering" method="get">
			<select name="orderby" class="orderby" aria-label="Shop order">
					<option value="menu_order" selected="selected">Default sorting</option>
					<option value="popularity">Sort by popularity</option>
					<option value="rating">Sort by average rating</option>
					<option value="date">Sort by latest</option>
					<option value="price">Sort by price: low to high</option>
					<option value="price-desc">Sort by price: high to low</option>
			</select>
				<input type="hidden" name="paged" value="1">
		</form>
	</div>
                    </div>
					<?php 
                   // endwhile;
                endif;
                ?>
                </div>
            </div>
	
        </section>

		

		<section class="baishnab-category-products-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-3"></div>
                    <div class="col-sm-12 col-md-12 col-lg-9 new-alignment">
				
					<div class=" category-page-products">

								<?php
								if ( woocommerce_product_loop() ) {

									/**
									 * Hook: woocommerce_before_shop_loop.
									 *
									 * @hooked woocommerce_output_all_notices - 10
									 * @hooked woocommerce_result_count - 20
									 * @hooked woocommerce_catalog_ordering - 30
									 */
									do_action( 'woocommerce_before_shop_loop' );

									woocommerce_product_loop_start();
									?>
									<!-- <div class="row"> -->
										

									
									<?php

									if ( wc_get_loop_prop( 'total' ) ) {
										while ( have_posts() ) {
											the_post();

											/**
											 * Hook: woocommerce_shop_loop.
											 */
											do_action( 'woocommerce_shop_loop' );
											?>
											<!-- <div class="col-md-4"> -->
											<?php

											wc_get_template_part( 'content', 'product' );
											?>
  <!-- </div> -->
											<?php
										}
									}
									?>
	                             
									<!-- </div> -->
									<?php

									woocommerce_product_loop_end();

									/**
									 * Hook: woocommerce_after_shop_loop.
									 *
									 * @hooked woocommerce_pagination - 10
									 */
									do_action( 'woocommerce_after_shop_loop' );
								} else {
									/**
									 * Hook: woocommerce_no_products_found.
									 *
									 * @hooked wc_no_products_found - 10
									 */
									do_action( 'woocommerce_no_products_found' );
								}
?>
					
</div>

<?php
								/**
								 * Hook: woocommerce_after_main_content.
								 *
								 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
								 */
								do_action( 'woocommerce_after_main_content' );




								?>



	
					</div>
					</div>
					</div>
			
					</section>
					<?php
get_footer( 'shop' );
