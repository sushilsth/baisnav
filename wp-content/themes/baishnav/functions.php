<?php
    function bs_theme_support(){
        //Add dynamic title tag support
        add_theme_support('title-tag');
        add_theme_support('custom-logo');
        add_theme_support('post-thumbnails');
        add_theme_support( 'woocommerce' );
        // add_theme_support( 'menus' );
    }
    
    add_action('after_setup_theme', 'bs_theme_support');

    function e_bs_menus(){

        $locations = array(
            'primary' => "Top Navigation",
            'middle' => "Middle Navigation",
            'footer' => "Footer Navigation",
            'footer' => "Footer Categories",
        );

        register_nav_menus($locations);
    }

    add_action('init', 'e_bs_menus');

    //remove <p> tag from the_content() method

    
    // function my_wp_content_function($content) {
    //     return strip_tags($content,"<br><h2>"); //add any tags here you want to preserve
    // }
        
   // add_filter('the_content', 'my_wp_content_function');


        function bs_register_style(){

        $version = wp_get_theme()->get('version');
        wp_enqueue_style( 'bs-styles', get_template_directory_uri() . "/assets/css/styles.css" , array(), $version, 'all');
        wp_enqueue_style( 'bs-detail-page-css', get_template_directory_uri() . "/assets/css/category.css" , array(), $version, 'all');
        wp_enqueue_style( 'contact-us-css', get_template_directory_uri() . "/assets/css/contact-us.css" , array(), $version, 'all');
        wp_enqueue_style( 'about-us-css', get_template_directory_uri() . "/assets/css/about-us.css" , array(), $version, 'all');
        wp_enqueue_style( 'bs-cart-page', get_template_directory_uri() . "/assets/css/cart-page.css" , array(), $version, 'all');
        wp_enqueue_style( 'bs-check-out-page', get_template_directory_uri() . "/assets/css/checkout-page.css" , array(), $version, 'all');
        wp_enqueue_style( 'bs-cat-css', get_template_directory_uri() . "/assets/css/detail.css" , array(), $version, 'all');
        wp_enqueue_style( 'bs-slider',"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" , array(), '1.9.0', 'all');
        wp_enqueue_style( 'bs-slider-theme',"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" , array(), '1.9.0', 'all');
        wp_enqueue_style( 'bs-navbar-bootstrap', "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" , array(), '4.6.0', 'all');
        wp_enqueue_style( 'bs-fancybox', "https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" , array(), '4.6.0', 'all');

    }
    add_action('wp_enqueue_scripts', 'bs_register_style');

    function register_script(){
        
        $version = wp_get_theme()->get('version');
        
        wp_enqueue_script( 'bs-navbar-jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" , array(), '3.6.0', true);
        wp_enqueue_script( 'baishnav-slider', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" , array(), '1.9.0', true);

        wp_enqueue_script( 'bs-popper', "https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" , array(), '1.16.1', true);
        wp_enqueue_script( 'bes-bootstrap', "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" , array(), '4.6.0', true);
        wp_enqueue_script( 'bes-product-slider', get_template_directory_uri() . "/assets/js/slider.js" , array(), '1.0', true);
        wp_enqueue_script( 'bs-detail-zoom-feature', "https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" , array(), '1.0', true);
        //<script src="" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      
        wp_enqueue_script( 'bs-navbar', get_template_directory_uri() . "/assets/js/navbar.js" , array(), '1.0', true);
        wp_enqueue_script( 'bs-quick-view', get_template_directory_uri() . "/assets/js/quick-view.js" , array(), '1.0', true);
        wp_enqueue_script( 'dp-dealers-map', get_template_directory_uri() . "/assets/js/dealerMap.js" , array(), '1.0', true);
        // wp_enqueue_script( 'dp-google-marker-cluster', "https://unpkg.com/@googlemaps/markerclustererplus/dist/index.min.js"
        // , array(), '', true);
        wp_enqueue_script( 'dp-google-map-api', "https://maps.googleapis.com/maps/api/js?key=AIzaSyByga3-oI5H0cHf3fmiI0FQA7MeFb7Q7Lg&callback=initMap"
         , array(), '', true);


    }
    add_action('wp_enqueue_scripts', 'register_script');

    include get_template_directory() . '/functions_woocommerce.php';



    
    ?>
    