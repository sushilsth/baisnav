
<?php

/**
 * Template Name: News and Events
 */

    get_header();

?>
<div class="durbar-our-products  new-background-color">
    <div class="news-single-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <div class="single-page-news-container">
                        <div class="single-page-news-image">
                            <img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0]; ?>" alt="">
                        </div>
                        <div class="single-page-news-content">
                            <h1 class = "single-page-news-title"><?php echo get_the_title(); ?></h1>
                            <p class = "single-page-news-date">01/29/2021</p>
                            <p class = "single-page-news-text">
									<?php echo the_content(); ?>
							</p>
                        </div>
						   <div class="df-news-share">
							 <a class = "single-page-news-button" href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"  target="_blank">Share</a>  
						</div>
                        <a class = "single-page-news-button" href="<?php echo home_url(); ?>/news-events/">Back to all news</a>
                    </div>
                <?php 
                        endwhile; 
                    endif; 
                ?>

                </div>
                <div class="col-lg-5">
                    <div class="single-page-related-news">
                        <div class="durbar-products-listings-heading single-news-pseudo-classes">
                            <h2>Related News</h2>
<!--                             <img src="<?php// echo get_template_directory_uri();?>/assets/images/royal.png" alt="" srcset=""> -->
                        </div>
                        <div class="single-page-related-news-container">
                            <?php
                                $paged = get_query_var('paged');
                                $args = array(
                                    'post_type' => 'news_and_events',
                                    'posts_per_page' => 4,
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'paged' => $paged,
                                    'post_status' => 'publish',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'news_and_events',
                                            'field'    => 'slug',
                                          
                                        ),
                                    ),
                                );
                                $the_query = new WP_Query($args);
                                if ($the_query->have_posts()) :
                                    while ($the_query->have_posts()) :
                                        $the_query->the_post();
                                        // if( have_rows('products') ) :
                                        //     while( have_rows('products') ) : the_row();
                            ?>

                            <div class="single-page-related-news-title">
                                <a href="<?php echo the_permalink(); ?>">
                                    <h1><?php echo get_the_title(); ?></h1>
                                </a>
                            </div>

                            <?php 
                                        //     endwhile;
                                        // endif;
                                    endwhile;
                                endif;
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php 
    get_footer();
?>