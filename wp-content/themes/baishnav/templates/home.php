
<?php
/**Template Name: Home */
    get_header();
?>


    <main>
        <section>
            <div class="baishnav-first-section-wrapper">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                            <div class="baishnav-first-section-slider-container ">
                                <div class="bs-slider bs-main-slider-text-content-wrapper">
                                <?php if( have_rows('hero_section_slider') ){
                                        while( have_rows('hero_section_slider') ) : the_row();      
                                ?>
                                                
                                                        <div class="bs-slider-image">
                                                        <div class="bs-main-slider-text-content">
                                                    <p>Top Sellings</p>
                                                    <h3 style=color:<?php echo get_sub_field('text_color'); ?>;><?php echo get_sub_field('hero_text'); ?></h3>
                                                    <!-- <button>Shop Now</button> -->
                                                    <a href="<?php echo get_sub_field('link'); ?>">Shop Now</a>
                                                </div>
                                    <img src="<?php echo get_sub_field('hero_image_slider'); ?>" alt="Slider Image">
                                    </div>
                                    <?php 
                                        endwhile;
                                        }
                                    ?>
                                </div>

                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                        <?php if( have_rows('hero_image') ){
                                        while( have_rows('hero_image') ) : the_row();      
                                ?>
                            <div class="bs-first-section-image">

                              <div class=" bes_css" >
                                 <div class="bs-banner-text">
                                    <p> <?php echo get_sub_field('hero_image1_tag'); ?></p>
                                    <h3><?php echo get_sub_field('hero_text_heading1'); ?></h3>
                                    <p><?php echo get_sub_field('hero_text1'); ?></p>
                                    <!-- <button>Shop Now > </button> -->
                                    <a href="<?php echo get_sub_field('hero_image1_link'); ?>">Shop Now</a>
                                 </div>
                            
                              <div class="baishnav-first-section-image-container">
                                        <img src="<?php echo get_sub_field('hero_image1'); ?>" alt="Slider Image">
                                    </div>
                                </div>

                                <div>
                                <div class="bs-banner-text">
                                    <p><?php echo get_sub_field('hero_image2_tag'); ?></p>
                                    <h3><?php echo get_sub_field('hero_text_heading2'); ?></h3>
                                    <p><?php echo get_sub_field('hero_text2'); ?></p>
                                    <!-- <button>Shop Now ></button> -->
                                    <a href="<?php echo get_sub_field('hero_image2_link'); ?>">Shop Now</a>
                                 </div>
                                    <div class="baishnav-first-section-image-container">
                                        <img src="<?php echo get_sub_field('hero_image2'); ?>" alt="Slider Image">
                                    </div>
                                </div>
                            </div>
                            <?php 
                                        endwhile;
                                        }
                                    ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="baishnav-shopby-category-wrapper">
                <h1>Shop By Category</h1>
                <div class="">
                <?php// if( have_rows('shop_by_category') ){
                                       // while( have_rows('shop_by_category') ) : the_row();      
                                ?>
                    <!-- <a href="<?php// echo get_sub_field('category1url'); ?>"> -->
                        <div class="baishnav-items-category baishnav-items-category-container">
                        <?php
            $taxonomyName = "product_cat";
            $prod_categories = get_terms($taxonomyName, array(
                'orderby'=> 'name',
                'order' => 'ASC',

            ));  
            // var_dump($prod_categories);

            foreach( $prod_categories as $prod_cat ) :

                if ( $prod_cat->slug == "uncategorized" ){
                    break;
                }

                if ( $prod_cat->parent != 0 )
                continue;
                $cat_thumb_id = get_woocommerce_term_meta( $prod_cat->term_id, 'thumbnail_id', true );
                $cat_thumb_url = wp_get_attachment_thumb_url( $cat_thumb_id );
                $term_link = get_term_link( $prod_cat, 'product_cat' );
                ?>
                <li class="bs-shop-by-category-card">

                <div style = "background: url(<?php echo $cat_thumb_url; ?>)" class="shop-by-category-card-img" >
                    <?php

            $product_categories = array($prod_cat->name);

            $wc_query = new WP_Query( array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'posts_per_page' => 6,
                'tax_query' => array( array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'slug',
                    'terms'    => $product_categories,
                    'operator' => 'IN',
                ) )
            ) );
            ?>

            <ul>
            <a href="<?php echo $term_link; ?>" class="bs_shop_by"><span> <?php echo $prod_cat->name; ?></span></a>
                <?php if ($wc_query->have_posts()) : ?>
                <?php while ($wc_query->have_posts()) :
                            $wc_query->the_post(); ?>
                <li>
                    <p>
                        <a href="<?php the_permalink(); ?>">
                        < <?php the_title(); ?>
                        </a>
                    </p>
                
                </li>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
                <?php else:  ?>
                <li> 
                    <?php _e( 'No Products' ); ?>
                </li>
                <?php endif; ?>
            </ul>
                
                    

            
                </li>
                <?php endforeach; 
            wp_reset_query();
            ?>




                        <?php //$thumb =// wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );?>
                            <?php
							//if (is_shop()){
								//echo('<img src="'.$thumb['0'].'" alt="">');
							//}else{
							//	echo do_shortcode('[categorythumbnail]');
							//}
							?>
                            <!-- <img src="<?php //echo get_sub_field('category1'); ?>" alt="Baishnab Food"  loading="lazy"> -->
                        </div>
                    <!-- </a> -->
           
                    <?php 
                                                //endwhile;
                                               // }
                                            ?>
                                        
                </div>
                 
            </div>
        </section>
        <section class="baishnab-home-our-products-wrapper">
            <div class="baishnab-home-featured-products-wrapper">
                <h1>Our Products</h1>
                <div class="baishnab-home-tabs ">
                    <ul class="nav " role="tablist">
                        <li class="nav-item">
                            <a href="#step-1" id="step1-tab" class="nav-link active" aria-selected="true"
                                data-toggle="tab" role="tab">
                                NEW PRODUCT
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#step-2" id="step2-tab" class="nav-link" aria-selected="false" data-toggle="tab"
                                role="tab">
                                SPECIAL PRODUCT
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#step-3" id="step3-tab" class="nav-link" aria-selected="false" data-toggle="tab"
                                role="tab">
                                BEST SELLER
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#step-4" id="step4-tab" class="nav-link" aria-selected="false" data-toggle="tab"
                                role="tab">
                                FEATURED PRODUCT
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="step-1" aria-labelledby="step-tab" role="tabpanel">

                            <div class="baishnab-home-featured-products-card-container baishnab-our-product-slider  baishnab-home-our-product-card ">
                                <!-- <div class="baishnab-home-featured-products-card "> -->
                                
                     
                     <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => -1,
                                'meta_value' => 'new product'
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                ?>
                                <?php
                                    '<div class="baishnab-home-featured-products-card "><div class="baishnab-home-featured-products-image">'.wc_get_template_part( 'content', 'product' ).
                                   '</div></div>';
                                    ?>
                                    <?php
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="step-2" aria-labelledby="step2-tab" role="tabpanel
                        ">
                            
                            <div
                                class="baishnab-home-featured-products-card-container baishnab-our-product-slider  baishnab-home-our-product-card ">
                                <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => -1,
                                'meta_value' => 'special product'
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                ?>
                                <?php
                                    '<div class="baishnab-home-featured-products-card "><div class="baishnab-home-featured-products-image">'.wc_get_template_part( 'content', 'product' ).
                                   '</div></div>';
                                    ?>
                                    <?php
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="step-3" aria-labelledby="step3-tab" role="tabpanel
                        ">

                         
                            <div
                                class="baishnab-home-featured-products-card-container baishnab-our-product-slider  baishnab-home-our-product-card ">
                                <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => -1,
                                'meta_value' => 'best seller'
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                ?>
                                <?php
                                    '<div class="baishnab-home-featured-products-card "><div class="baishnab-home-featured-products-image">'.wc_get_template_part( 'content', 'product' ).
                                   '</div></div>';
                                    ?>
                                    <?php
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>

                            </div>

                        </div>
                        <div class="tab-pane fade" id="step-4" aria-labelledby="step4-tab" role="tabpanel
                        ">
                        
                            <div
                                class="baishnab-home-featured-products-card-container baishnab-our-product-slider  baishnab-home-our-product-card ">
                                <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => 12,
                                'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_visibility',
                                            'field'    => 'name',
                                            'terms'    => 'featured',
                                        ),
                                    ),
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                ?>
                                <?php
                                    '<div class="baishnab-home-featured-products-card "><div class="baishnab-home-featured-products-image">'.wc_get_template_part( 'content', 'product' ).
                                   '</div></div>';
                                    ?>
                                    <?php
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

     <section>
        <div class="baishnav-home-serves-happiness-wrapper">

            <?php
            $rating = get_post_meta( $product, '_wc_average_rating', true );
            $comments = get_comments( array(
                'post_id' => $product
            ));
           ?>
           <?php if($rating || $comments){ ?>
            <h1>Baishnab Serves Happiness</h1>
            <?php  } ?>

      
           <div class="feedback-wrapper-baishnav feedback-slider-baishnab">
            <?php 
            $args = array( 
                'status'      => 'approve', 
                'post_status' => 'publish', 
                'post_type' => 'product',
            //    'meta_key' => '_wc_average_rating',
              
            );
            $loop = new WP_Query( $args );

            while ( $loop->have_posts() ) : $loop->the_post();
            $product =  get_the_ID();

            $rating = get_post_meta( $product, '_wc_average_rating', true );
            $comments = get_comments( array(
                'post_id' => $product
            ) );
            $starImageUrl = get_template_directory_uri() . "/assets/images/star32.svg";
            $blankStar =  get_template_directory_uri() . "/assets/images/white-star.svg";
            $adminImageUrl = get_template_directory_uri() . "/assets/images/smile.png";
            foreach( $comments as $comment ) :
           ?>
                
                     <div class="feedback-section-baishnav">
                     <div class="baishnav-serves-happiness-card-container">
                     <div class="baishnav-ratings-container">
                        <div class="baishnav-smile-image">
                         <img src="<?php echo $adminImageUrl ?>" alt="Smile Picture"   loading="lazy">
                        </div>
                     <div class="baishnav-rating-header">
                        <p>By <?php echo  $comment->comment_author ?></p>
                     <div class="rating-star-image-container">
                         <?php
                     if($rating){
                             for($i=1; $i<=5; $i++){
                                if($i<=$rating){
                                    echo('
                                    <div class="rating-star-image detail-page-review-star">
                                        <img src="'.$starImageUrl.'" alt="Ratings" loading="lazy">
                                    </div>
                                        ');
                                    }
                                    else{
                                        echo('
                                        <div class="rating-star-image detail-page-review-star">
                                        <img src="'.$blankStar.'" alt="Ratings" loading="lazy">
                                        </div>
                                        ');
                                    }
                                
                              } 
                         }
                          ?>
                          </div>
                          </div>
                          </div>

                     </div>
                     <div class="baishnav-feedback-text">
                         <p> <?php echo $comment ->comment_content ?> </p>
                      
                     </div>
                 </div>
                 <?php
                endforeach;
       
        endwhile;
    ?>
     <?php
            $comments = get_comments( $args );
          
             ?>
                   </div>
            </div>
        </section>

        <section>
            <div class="baishnab-home-featured-products-wrapper">
                <?php 
                  $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 12,
                    'tax_query' => array(
                            array(
                                'taxonomy' => 'product_visibility',
                                'field'    => 'name',
                                'terms'    => 'featured',
                            ),
                        ),
                    );
                $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                ?>
                <h1>Featured Products</h1>
                <?php
                            }
                            ?>
                            
                <div
                    class="baishnab-home-featured-products-card-container baishnab-featured-product-slider bs-featured">
                   
                    <?php
                          
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                    wc_get_template_part( 'content', 'product' );
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>

                </div>
            </div>
        </section>
        <section class="baishnab-home-recent-news-section">
            <div class="baishnab-home-recent-news-wrapper">
                <h1>
                    Recent News
                </h1>
                <div class="baishnab-home-recent-news-card-conatiner">
                <?php $args = array(  
         'post_type' => 'news_and_events',
         'post_status' => 'publish',
         'posts_per_page' => 4, 
         'orderby' => 'title', 
         'order' => 'ASC',
     );
     $loop = new WP_Query( $args ); 
     while ( $loop->have_posts() ) : $loop->the_post(); ?>
    
                    <a href="<?php the_permalink(); ?>">
                        <div class="baishnab-home-recent-news-card">
                            <div class="baishnab-home-recent-news-image">
                            <?php if (has_post_thumbnail( $post->ID ) ): ?>
                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

                <?php endif; ?>
                 
                             <img src="<?php echo $image[0]; ?>"alt=" Baishnav news"  loading="lazy"> 
                            </div>
                            <div class="baishnab-home-recent-news-description">
                                <h1> <?php the_title(); ?></h1>
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                    </a>
                    <?php  endwhile ;
               wp_reset_postdata();
                    ?>
                </div>
            </div>
        </section>

        <!-- <section class="second-last-section-baishnab">
            <div class="conatiner-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="baishnab">

                        </div>
                    </div>
                    <div class="col-md-4">
                          <div class="baishnab">
                            
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
       

    </main>


<?php
get_footer();
?>