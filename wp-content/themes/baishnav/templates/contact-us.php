<?php
/**Template Name: Contact Us */

?>
<?php
get_header();
?>
<div class="bs-b">
<?php
                    if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                    ?>
</div>

    <div class="contact-us-form-container-wrappper">
        <div class="contact-us-form-container">
            <div class="row">
                <div class="col-sm-12 col-md-7 col-lg-7">
                    <h2>Our Location On Google Map</h2>
                  <!-- <div class="row">
                  <?php if( have_rows('contact_us_first_section') ){
                    while( have_rows('contact_us_first_section') ) : the_row();      
                    ?>
                      <div class="col-md-6 col-lg-6">
                      <div class="bs-branches">
                
                        <div class="contact-us-detail-container">
                           <div class="bs-contact-map-detail">
                                <div class="bs-map-icon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/images/pin.svg" alt="">
                                </div>
                               <a href="<?php echo get_sub_field('branche_url'); ?>"> <?php echo get_sub_field('branches'); ?></a>
                        
                            </div>
                            <div class="bs-contact-map-detail">
                                <div class="bs-phone-icon">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/images/phone.svg" alt="">
                                </div>
                               <p ><?php echo get_sub_field('contact_number'); ?></p>
                        
                            </div>
                        </div>
                    
                    </div>
                      </div>
                      <?php 
                    endwhile;
                }
                ?>
                  </div>
                   -->
              
                    <div class="bs-contact-map-conatiner">
                   
                    <!-- <h3>Our Location on Google Map</h3> -->

                            <!-- <?php 
                            $cordinateData = get_field('google_map');
                            $cordinate = explode(', ', $cordinateData);
                            $lat = $cordinate[0];
                            $lang = $cordinate[1];
                            ?> -->

                              <!-- <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" 
                              marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=520&amp;hl=en&amp;q=<?php echo $lat; ?>,%20<?php echo $lang; ?>+(My%20Business%20Name)&amp;t=&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe> -->



                              <div class="bs-branch-intro">
                                    <div class="b-outlets">
                                        <div id="map" class="bs-map"></div>
                                    </div>
                               </div>


                </div>
                </div>

                
              
                 <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="bs-contact-form">
                    <?php echo do_shortcode('[contact-form-7 id="199" title="Contact Us"]'); ?>

                    </div>
                    
        
                </div> 
            </div>
        </div>

    </div>


<?php
 get_footer();

 ?>