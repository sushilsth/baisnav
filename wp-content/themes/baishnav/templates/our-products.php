<?php
/**
 * Template Name: Our Products
 */
get_header();
?>

<div class="bs-b">
<?php
                    if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                    ?>
</div>

<main class="bs-body-wrapper">
    <div class="b-image">
        <div class="bs-our-product-left-img">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/m2p.png" alt="Baishnab Sweets">
        </div>
        <div class="bs-our-product-right-img">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/m3.png" alt="Baishnab Sweets">
        </div>
        <div class="b-our-products-content">
        <h1>Our Products</h1>
        <p>Baishnab Sweets Products are well recognized and known for its Quality. Basket of Baishnab Sweets
             products satisfy felt need of our customers through out the year and in all seasons .
              During festive seasons Baishnab Sweets gift packs ,unique in design coupled with special 
              products revive emotional and personal relationship as well as the social bond with our valuable 
              customer.</p>
        </div>
        
    </div>

    <section class="baishnab-home-our-products-wrapper bpadding">
            <div class="baishnab-home-featured-products-wrapper">
                <div class="baishnab-home-tabs ">
                    <ul class="nav " role="tablist">
                        <li class="nav-item">
                            <a href="#step-1" id="step1-tab" class="nav-link active" aria-selected="true"
                                data-toggle="tab" role="tab">
                                NEW PRODUCT
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#step-2" id="step2-tab" class="nav-link" aria-selected="false" data-toggle="tab"
                                role="tab">
                                SPECIAL PRODUCT
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#step-3" id="step3-tab" class="nav-link" aria-selected="false" data-toggle="tab"
                                role="tab">
                                BEST SELLER
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#step-4" id="step4-tab" class="nav-link" aria-selected="false" data-toggle="tab"
                                role="tab">
                                FEATURED PRODUCT
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="step-1" aria-labelledby="step-tab" role="tabpanel">

                            <div class="baishnab-home-featured-products-card-container baishnab-our-product-slider  baishnab-home-our-product-card ">
                                <!-- <div class="baishnab-home-featured-products-card "> -->
                                
                     
                     <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => -1,
                                'meta_value' => 'new product'
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                ?>
                                <?php
                                    '<div class="baishnab-home-featured-products-card "><div class="baishnab-home-featured-products-image">'.wc_get_template_part( 'content', 'product' ).
                                   '</div></div>';
                                    ?>
                                    <?php
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="step-2" aria-labelledby="step2-tab" role="tabpanel
                        ">
                            
                            <div
                                class="baishnab-home-featured-products-card-container baishnab-our-product-slider  baishnab-home-our-product-card ">
                                <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => -1,
                                'meta_value' => 'special product'
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                ?>
                                <?php
                                    '<div class="baishnab-home-featured-products-card "><div class="baishnab-home-featured-products-image">'.wc_get_template_part( 'content', 'product' ).
                                   '</div></div>';
                                    ?>
                                    <?php
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="step-3" aria-labelledby="step3-tab" role="tabpanel
                        ">

                         
                            <div
                                class="baishnab-home-featured-products-card-container baishnab-our-product-slider  baishnab-home-our-product-card ">
                                <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => -1,
                                'meta_value' => 'best seller'
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                ?>
                                <?php
                                    '<div class="baishnab-home-featured-products-card "><div class="baishnab-home-featured-products-image">'.wc_get_template_part( 'content', 'product' ).
                                   '</div></div>';
                                    ?>
                                    <?php
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>

                            </div>

                        </div>
                        <div class="tab-pane fade" id="step-4" aria-labelledby="step4-tab" role="tabpanel
                        ">
                        
                            <div
                                class="baishnab-home-featured-products-card-container baishnab-our-product-slider  baishnab-home-our-product-card ">
                                <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => 12,
                                'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_visibility',
                                            'field'    => 'name',
                                            'terms'    => 'featured',
                                        ),
                                    ),
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();
                                ?>
                                <?php
                                    '<div class="baishnab-home-featured-products-card "><div class="baishnab-home-featured-products-image">'.wc_get_template_part( 'content', 'product' ).
                                   '</div></div>';
                                    ?>
                                    <?php
                                endwhile;
                            } else {
                                echo __( 'No products found' );
                            }
                            wp_reset_postdata();
                ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

 </main>
                        <?php
 get_footer();
 ?>