<?php
/**
 * Template Name: About Us
 */
get_header();
?>
<div class="bs-b">
<?php
                    if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                    ?>
</div>
 <main class="bs-ap">
        <section class="bs-about-first-section">
                        <div class="bs-about-description">
                            <h2>What we Do?</h2>
                            <p class="bs-au-description"><?php echo get_field("company_intoduction"); ?></p>
                                <div class="bs-about-image">
                                <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                                </div>
                          <div class="bs-au-detail-description">
                          <?php echo the_content(); ?>
                          </div>
                              
                        </div>
        </section>
        <section class="bs-about-second-section">
            <div class="bs-about-second-section-container">
            <div class="container-fluid">
                <div class="row no-gutters">
                <?php if( have_rows('company_information') ){
                                        while( have_rows('company_information') ) : the_row();      
                                ?>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
               
                        <div class="years-in-bussiness">
                          <div class="years-in-bussiness-detail">
                            <p><?php echo get_sub_field("business_details"); ?></p>
                             <span><?php echo get_sub_field("business_detail_info"); ?></span>
                            </div>
                        </div>
                      
                    </div>
                    <?php 
                                        endwhile;
                                        }
                                    ?>
                    <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                            <div class="years-in-bussiness">
                            <div class="years-in-bussiness-detail">
                            <p>21</p>
                             <span>Years in bussiness</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                            <div class="years-in-bussiness">
                            <div class="years-in-bussiness-detail">
                            <p>21</p>
                             <span>Years in bussiness</span>
                            </div>
                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                            <div class="years-in-bussiness">
                            <div class="years-in-bussiness-detail">
                            <p>21</p>
                             <span>Years in bussiness</span>
                            </div>
                            </div>
                    </div> -->
                  </div>
                </div>
          
            
             
             

            </div>

        </section>

 
 </main>







<?php
get_footer();
?>