<?php
/**
 * Template Name: Our Branches
 */
get_header();
?>

<div class="bs-b">
<?php
           if ( function_exists('yoast_breadcrumb') ) {
           yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                                                       }
         ?>
</div>


<section>
    <div class="bs-branches-conatiner-wrapper">
        <h1>Outlets</h1>
        <div class="bs-branch-intro">
            <div class="b-outlets">
                <div id="map"></div>
            </div>

        </div>

     
        
        <div class="bs-branches-card-conatiner">
            <div class="container-fluid">
                <div class="row">
                <?php if( have_rows('our_branches') ){
                                        while( have_rows('our_branches') ) : the_row();      
                                ?>
                
                <div class="col-md-6 col-lg-4 mb-4">
                              <?php if( have_rows('branch') ){
                                        while( have_rows('branch') ) : the_row();      
                                ?>
                    <div class="bs-branch-card">
                        <span>Open</span>
                                <div class="bs-branch-image">
                                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/default-store-banner.png" alt="Baishnab Sweets">
                                    <div class="bc-branch-content">
                                        <h2>
                                            <?php echo get_sub_field("branch_name"); ?>
                                        </h2>
                                        <p><?php echo get_sub_field("address"); ?></p>
                                        <div class="bs-contact-container">
                                            <div class="bs-phone-icon">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone.svg" alt="Baishnab Sweets">
                                            </div>
                                            <p><?php echo get_sub_field("contact_info"); ?></p>
                                        </div>
                                    </div>
                               </div>
                        
                                <div class="bs-branch-avtar">
                                        <img src="<?php echo get_sub_field("branch_image"); ?>" alt="Baishnab Sweets">
                                </div>
                       </div>
                 </div>
               <?php endwhile; } ?>
               <?php 
              endwhile;
                                        }
            ?>

             </div>
           </div>
         </div>
      </div>   
    </div>
</section>



<?php
get_footer();
?>