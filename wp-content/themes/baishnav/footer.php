<footer>
        <div class="baishnab-footer-wrapper">
            <div class="baishnab-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 bsflm">
                        <div class="baishnab-footer-logo-container">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/baishnab_logo.png" alt="Baishnab logo">
                            </div>
                        <div class="bs-contact-detail-footer">
                                <div class="bs-phone-icon-footer">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/images/pin-copy.svg" alt="Baishnab logo">
                                </div>
                                <p> Bharatpur-10 Chitwan (Nepal)
                                </p>
                        </div>

                        <div class="bs-contact-detail-footer">
                            <div class="picon-footer">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/phone-copy.svg" alt="">
                            </div>
                            <a href="telno:+977-071500061" style=" font-size: 13px;" >+977-056525026 / +977-056533652
                            </a>


                        </div>
                        </div>
                        <div class=" col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="bs-branches-footer">
                            <h3>My Account</h3>
                            <?php 
                        wp_nav_menu(
                            array(
                                'menu' => 'footer',
                                'container' => '',
                                'theme_location' => 'footer'
                            )
                        );
                    ?>
                        </div>
                           
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="bs-branches-footer">
                                <h3>Services</h3>
                                <p> <a href="<?php echo get_home_url(); ?>/about-us">About Us</a></p>
                               <p><a href="<?php echo get_home_url(); ?>/contact-us">Contact Us</a></p>
                                <p>  <a href="<?php echo get_home_url(); ?>/our-branches">Our Restaurants</a></p>
                                <p> <a href="<?php echo get_home_url(); ?>/news-and-events">News and Events</a></p>
                              
                               

                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="footer-category">
                            <h3>Category</h3>
                            <?php 
                        wp_nav_menu(
                            array(
                                'menu' => 'Footer Categories',
                                'container' => '',
                                'theme_location' => 'footer'
                            )
                        );
                    ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <p>
            copyright @ <a href="">bihanitech </a> rights reserved</p>

    </footer>

</body>

<?php
wp_footer();
?>
</html>