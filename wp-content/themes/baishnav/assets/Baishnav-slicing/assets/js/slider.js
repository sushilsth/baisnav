$('.bs-slider').slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    
  });


  // baishnav serves happiness slider

  $('.feedback-slider-baishnab').slick({
    dots: true,
    infinite: true,
    autoplay:false,
    slidesToShow: 3,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 668,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  // featured product slider

  $('.baishnab-featured-product-slider').slick({
    dots: true,
    infinite: true,
    autoplay:false,
    slidesToShow: 5,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 1380,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 950,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }

    ]
  });


   // our product slider


   $('.baishnab-our-product-slider').slick({
    dots: true,
    slidesPerRow: 5,
    rows: 2,
    responsive: [
      {
        breakpoint: 1500,
        settings: {
          slidesPerRow: 4,
          rows: 2,
        }
      },
      {
        breakpoint: 1250,
        settings: {
          slidesPerRow: 3,
          rows: 2,
        }
      },
      {
        breakpoint: 976,
        settings: {
          slidesPerRow: 2,
          rows: 2,
        }
      },
      {
        breakpoint: 700,
        settings: {
          
          slidesPerRow: 1,
          rows: 1,
        }
      },
    {
      breakpoint: 478,
      settings: {
        slidesPerRow: 1,
        rows: 1,
      }
    }
  ]
});

  $('.baishnab-our-product-slider').slick({
    dots: true,
    infinite: true,
    autoplay:false,
    slidesToShow: 5,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 1380,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 950,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

   $('.detail-page-slider').slick({
    dots: false,
    infinite: true,
    autoplay:false,
    slidesToShow: 4,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 1380,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 950,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 450,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  $(document).ready(function(){
    $(".baishnab-detail-page-first-wrapper .baishnab-detail-page-second-wrapper .small-img .product-small-image > img").click(function(){
var $smallImages = $(this).attr('src');
$('.bs-product-image-container>img').attr('src', $smallImages);
    })
  })