// Adding icon to the button
$(document).ready(function () {
  $(".quick_view span").html(
    "<img title='Quick view' src='" +
      theme_directory +
      "/assets/images/view-copy.svg' />"
  );
  // $(".add_to_cart_button").html(
  //   "<img title='Add to cart' src='" +
  //     theme_directory +
  //     "/assets/images/cart.png' />"
  // );
  



  // single_add_to_cart_button button alt disabled wc-variation-selection-needed

  // open bucket cart when cart button clicked
  $(".add_to_cart_button").on("click", function(){
    $(".xoo-wsc-modal ").addClass("xoo-wsc-cart-active");
  });

// add slider when quick view clicked

  $(".quick_view").on("click", function () {
    setTimeout(function () {
  
      $(".remodal .thumbnails.columns-3").slick({
        dots: false,
        infinite: true,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1380,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
              infinite: true,
             
            },
          },
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
         
            },
          },
          {
            breakpoint: 450,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            },
          },
        ],
      });
      console.log("quick view clicked");
    }, 2000);
  });


  // if ($(".bs-product-detail-wrapper").hasClass("product_title")) { 
  //   // $(".yith-wcwl-add-button").css("transform", "translate(50%, -50%)");
  //   // $(".yith-wcwl-add-button").css("bottom", "50%");
  //   console.log("hello");
  // }


  // if ($("#about").hasClass("opened")) {
  //   $("#about").animate({right: "-700px"}, 2000);
  // }
 
});


let mainWrapper = document.querySelector(".bs-product-detail-wrapper");
let variationForm = document.querySelector(".variations_form")
let wishlistButton = document.querySelector(".yith-wcwl-add-button");
let Buttons =document.querySelector(".bs-button-collection");
let QuickViewIcon = document.querySelector(".product .quick_view");
let AjaxaddToCart  = document.querySelector(".ajax_add_to_cart");
let BsProduct = document.querySelector(".product");


if (mainWrapper.contains(AjaxaddToCart)){
  console.log("has price");
}else{
  wishlistButton.style.display = "none";
}


if (mainWrapper.contains(variationForm)) {
  wishlistButton.style.transform = "translate(50%, 136%)";
  console.log("has variation");
}else{
  wishlistButton.style.transform = "translate(50%, -50%)";
  wishlistButton.style.bottom = "50%";
  Buttons.style.marginTop = "59px";
  console.log("has no variation");
}





