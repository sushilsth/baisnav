
var map;
var InfoObj = [];
  var markersOnMap = [
  
{

  placeName: '<strong>Baishnab Sweets Mega Outlet</strong><br>\
 Chaubiskothi, Bharatpur, Chitwan<br>',
  LatLng: [{
      lat:  27.680726970712623,
      lng:  84.43044451795141,

  }],
},
{

  placeName: '<strong>Baishnav Misthanna Bhandar</strong><br>\
  Sahid Chowk, Narayangarh, Chitwan',
  LatLng: [{
      lat: 27.693589081187618,
      lng: 84.42390228371131,
       
  }],
},

{

  placeName: '<strong>Vaishnab Misthanna Bhandar</strong><br>\
  Ratnanagar-2 Tandi, Chitwan',
  LatLng: [{
      lat: 27.622027771952364,
      lng: 84.51538861865853,
  }],
},

{

  placeName: '<strong>Baishnab Sweets</strong><br>\
 Campus Gate, Bharatpur, Chitwan',
  LatLng: [{
      lat: 27.687029987707508,
      lng: 84.4294852546001,

       
       
  }],
},


  ]
  var markers = [];
  function addMarkerInfo(){
      for(var i = 0; i< markersOnMap.length; i++){
        // var contentString = '<strong>' + markersOnMap[i].placeName + '</strong>' + '<br>' + markersOnMap[i].dealerName;
        var contentString =  markersOnMap[i].placeName;
        const marker = new google.maps.Marker({
              position: markersOnMap[i].LatLng[0],
             // icon: icons[markersOnMap[i].type].icon,
              map: map
          });
          markers.push(marker);
          const infowindow = new google.maps.InfoWindow({
              content: contentString
          });
          marker.addListener('click', function(){
            closeOtherInfo();
              infowindow.open(marker.get("map"), marker);
              InfoObj[0] = infowindow;
          });
      }
  }
function closeOtherInfo(){
  if(InfoObj.length > 0){
    InfoObj[0].set("marker", null);
    InfoObj[0].close();
    InfoObj[0].length = 0;
  }
}

// Initialize and add the map
function initMap() {
    // The location of Chitwan 
    const uluru = { lat:   27.676613452708832, lng: 84.45311095342535 };
    // The map, centered at Chitwan
    map = new google.maps.Map(document.getElementById("map"), {
      zoom: 12,
      center: uluru,
    });
    // The marker, positioned at Uluru
    // const marker = new google.maps.Marker({
    //   position: uluru,
    //   map: map,
    // });
  
        addMarkerInfo();
      }
   
      
       