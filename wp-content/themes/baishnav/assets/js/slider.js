$(".bs-slider").slick({
  dots: true,
  autoplay: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
});

// baishnav serves happiness slider

$(".feedback-slider-baishnab").slick({
  dots: true,
  infinite: true,
  autoplay: false,
  slidesToShow: 3,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 668,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
});

// featured product slider

$(".baishnab-featured-product-slider").slick({
  dots: true,
  infinite: true,
  autoplay: false,
  slidesToShow: 5,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 1500,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 1250,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 950,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
});

// our product slider

$(".baishnab-our-product-slider").slick({
  dots: true,
  // slidesToScroll: 1,
  slidesPerRow: 5,
  rows: 2,
  responsive: [
    {
      breakpoint: 1500,
      settings: {
        slidesPerRow: 4,
        rows: 2,
      },
    },
    {
      breakpoint: 1250,
      settings: {
        slidesPerRow: 3,
        rows: 2,
      },
    },
    {
      breakpoint: 976,
      settings: {
        slidesPerRow: 2,
        rows: 2,
      },
    },
    {
      breakpoint: 560,
      settings: {
        slidesPerRow: 1,
        rows: 1,
      },
    },
  ],
});

$(".detail-page-slider").slick({
  dots: false,
  infinite: true,
  autoplay: false,
  slidesToShow: 4,
  focusOnSelect: true,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 1380,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 950,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 450,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
});




//shop by category slider
$(".baishnav-items-category-container").slick({
  dots: false,
  infinite: true,
  autoplay: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1380,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        dots: false,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false,
      },
    },
    {
      breakpoint: 950,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
});



//related product section slider
$(".baishnab-detail-page-fourth-wrapper ul").slick({
  dots: false,
  infinite: true,
  autoplay: false,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1250,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false,
      },
    },
    {
      breakpoint: 950,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 650,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
});

$(document).ready(function () {


  // chnaging the main image when clicked on thumbnaill
  $(
    ".trying-to-wrap-the-image .small-img .product-small-image img"
  ).click(function () {
    console.log("hello");
    var smallImages = $(this).attr("src");
    //$(".bs-product-image-container img").attr("src", $smallImages);
    var image = $(".main-img> .bs-product-image-container> .woocommerce-product-gallery__image> img");
    console.log(smallImages);
    image.fadeOut('fast', function () {
       image.attr('src', smallImages);
       image.attr('srcset', smallImages);
       
  
    });
     image.one("load",function(){
       image.fadeIn('fast');
     });
  });




// resetting the position of tabs when switched
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $('.baishnab-our-product-slider').slick('setPosition');
  })
});


