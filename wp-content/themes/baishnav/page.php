<?php
get_header();
?>
<main>
    <section class="bs-cart-page-wrapper">
    <?php
                if(have_posts()){
                    while(have_posts()){
                        the_post();
                    }
                }

                ?>
                <h2><?php the_title(); ?></h2>
                <p><?php the_content(); ?></p>
    </section>
               
</main>


<?php
get_footer();
?>