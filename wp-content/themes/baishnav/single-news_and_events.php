<?php
get_header();
?>

<div class="bs-b">
<?php
           if ( function_exists('yoast_breadcrumb') ) {
           yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                                                       }
         ?>
</div>


<section>
    <div class="bs-branches-conatiner-wrapper">
		<h1 class = "bs-news-events-title"><?php the_title(); ?></h1>
		<?php the_content(); ?>
      </div>   
</section>



<?php
get_footer();
?>