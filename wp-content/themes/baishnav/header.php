<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head();?>
</head>

<body>
    <header>
    <?php if( is_front_page() ){?>
        <div class="baishnav-home-nav-wrapper">
            <div class="bs-top-mid-nav-wrapper">

           
            <!-- top navbar -->
            <div class="baishnav-home-top-navbar-container "  id="nav-ul">
                <div class="baishnav-home-top-navbar">
                    <div class="bs-bulk-order">
                        <a href="<?php echo get_home_url(); ?>/order-now">
                            <p>Click for <span>BULK ORDERS</span> </p>
                        </a>
                    </div>
                    
                    <div class="bs-top-nav-item ">
                    <?php 
            wp_nav_menu(
                array(
                    'menu' => 'primary',
                    'container' => '',
                    'theme_location' => 'primary',
                    'menu_class' => 'navbar-nav-ul',
                    'menu_id' => 'navbar-nav-ul',
                )
            );
        ?>
                        
                       
                    </div>
                    <div class="mobile-wishlist"><a href="<?php echo get_home_url(); ?>/wishlist">MY WISHLIST</a></div>

                    <div id="searchBar">
                    
                    <?php echo do_shortcode('[fibosearch]'); ?>
                        <!-- <input type="text" placeholder="Search Product" class="baishnab-seach-box"> -->
                        <!-- <button type="submit"> <img src="<?php// echo get_template_directory_uri();?>/assets/images/loupe.svg" alt="search"> </button> -->
                    </div>
                </div>
            </div>
            <!-- Middle navbar -->
            <div class="baishnav-home-mid-navbar-container">
                <!-- <a href=""> -->
                    <div class="baishnav-home-mid-navbar-logo">
                    <?php if (function_exists('the_custom_logo')){
                the_custom_logo();
            }
        ?>
                        <!-- <img src="./assets/images/baishnab_logo.png" alt="Baishnab logo"> -->
                    </div>
                <!-- </a> -->
            
                <div class="baishnav-searchbar-container">
                    
                <?php echo do_shortcode('[fibosearch]'); ?>
                    <!-- <input type="text" placeholder="Search Product" class="baishnab-seach-box"> -->
                    <!-- <button type="submit"> <img src="<?php// echo get_template_directory_uri();?>/assets/images/loupe.svg" alt="search"> </button> -->
                </div>
              
             <a href="<?php echo get_home_url(); ?>/wishlist" class="mid-nav-heart-icon">
       
                    <div class="baishnav-login-image heart-first-img">
                        <img  title="View Wishlist" src="<?php echo get_template_directory_uri();?>/assets/images/heart.svg" alt="Cart image">
                    </div>
                    <div class="baishnav-login-image heart-second-img">
                        <img  title="View Wishlist" src="<?php echo get_template_directory_uri();?>/assets/images/heart-c.svg" alt="Cart image">
                    </div>
                </a>
               
                <a href="<?php echo get_home_url(); ?>/cart" class="mid-nav-cart-icon">
               
                    <div class="baishnav-login-image cart-first-img">
                        <img  title="View Cart Page" src="<?php echo get_template_directory_uri();?>/assets/images/cart.svg" alt="Cart image">
                    </div>
                    <div class="baishnav-login-image cart-second-img">
                        <img  title="View Cart Page" src="<?php echo get_template_directory_uri();?>/assets/images/red-cart.svg" alt="Cart image">
                    </div>
                </a>
                <div class="hamburger" id="hamburger">
                    <img  src="<?php echo get_template_directory_uri();?>/assets/images/menu.svg" alt="menu">
                </div>


            </div>
            </div>
            <!-- bottom navbar -->
            <div class="baishnav-home-bottom-navbar-conatiner">
                <div class="baishnav-home-bottom-navbar-items">
                    <!-- <ul> -->
                    <?php 
            wp_nav_menu(
                array(
                    'menu' => 'middle',
                    'container' => '',
                    'theme_location' => 'middle',
                   
                )
            );
        ?>
                </div>
                <div class="baishnav-navbar-contact-container">
                    <a href="tel: 056596026" class="icon-tel-bs">
                    <div class="nav-telephone-image">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/telephone.svg" alt="Telephone image">
                    </div>
                </a>
                    <a href="tel: 056596026"> <span>Call us</span> :
                        056596026</a>
                </div>
            </div>

        </div>
        <?php 
    }else {
    ?>

<div class="baishnav-home-nav-wrapper">
            <!-- <div class="bs-top-mid-nav-wrapper"> -->

           
            <!-- top navbar -->
            <div class="baishnav-home-top-navbar-container "  id="nav-ul">
                <div class="baishnav-home-top-navbar">
                    <div class="bs-bulk-order">
                        <a href="<?php echo get_home_url(); ?>/order-now">
                            <p>Click for <span>BULK ORDERS</span> </p>
                        </a>
                    </div>
                    
                    <div class="bs-top-nav-item ">
                    <?php 
            wp_nav_menu(
                array(
                    'menu' => 'primary',
                    'container' => '',
                    'theme_location' => 'primary',
                    'menu_class' => 'navbar-nav-ul',
                    'menu_id' => 'navbar-nav-ul',
                )
            );
        ?>
                        
                       
                    </div>
                    <div class="mobile-wishlist"><a href="<?php echo get_home_url(); ?>/wishlist">MY WISHLIST</a></div>

                    <div id="searchBar">
                    
                    <?php echo do_shortcode('[fibosearch]'); ?>
                        <!-- <input type="text" placeholder="Search Product" class="baishnab-seach-box"> -->
                        <!-- <button type="submit"> <img src="<?php// echo get_template_directory_uri();?>/assets/images/loupe.svg" alt="search"> </button> -->
                    </div>

                </div>
            </div>
            <!-- Middle navbar -->
            <div class="baishnav-home-mid-navbar-container">
                <!-- <a href=""> -->
                    <div class="baishnav-home-mid-navbar-logo">
                    <?php if (function_exists('the_custom_logo')){
                the_custom_logo();
            }
        ?>
                        <!-- <img src="./assets/images/baishnab_logo.png" alt="Baishnab logo"> -->
                    </div>
                <!-- </a> -->
            
                <div class="baishnav-searchbar-container">
                    
                <?php echo do_shortcode('[fibosearch]'); ?>
                    <!-- <input type="text" placeholder="Search Product" class="baishnab-seach-box"> -->
                    <!-- <button type="submit"> <img src="<?php// echo get_template_directory_uri();?>/assets/images/loupe.svg" alt="search"> </button> -->
                </div>
              
             <a href="<?php echo get_home_url(); ?>/wishlist" class="mid-nav-heart-icon">
       
                    <div class="baishnav-login-image heart-first-img">
                        <img  title="View Wishlist" src="<?php echo get_template_directory_uri();?>/assets/images/heart.svg" alt="Cart image">
                    </div>
                    <div class="baishnav-login-image heart-second-img">
                        <img  title="View Wishlist" src="<?php echo get_template_directory_uri();?>/assets/images/heart-c.svg" alt="Cart image">
                    </div>
                </a>
               
                <a href="<?php echo get_home_url(); ?>/cart" class="mid-nav-cart-icon">
               
                    <div class="baishnav-login-image cart-first-img">
                        <img  title="View Cart Page" src="<?php echo get_template_directory_uri();?>/assets/images/cart.svg" alt="Cart image">
                    </div>
                    <div class="baishnav-login-image cart-second-img">
                        <img  title="View Cart Page" src="<?php echo get_template_directory_uri();?>/assets/images/red-cart.svg" alt="Cart image">
                    </div>
                </a>
                <div class="hamburger" id="hamburger">
                    <img  src="<?php echo get_template_directory_uri();?>/assets/images/menu.svg" alt="menu">
                </div>


            </div>
            <!-- </div> -->
            <!-- bottom navbar -->
            <div class="baishnav-home-bottom-navbar-conatiner">
                <div class="baishnav-home-bottom-navbar-items">
                    <!-- <ul> -->
                    <?php 
            wp_nav_menu(
                array(
                    'menu' => 'middle',
                    'container' => '',
                    'theme_location' => 'middle',
                   
                )
            );
        ?>
                        <!-- <li>
                            <a href="#"> Home</a>
                        </li>
                        <li>
                            <a href="#"> Shop</a>
                        </li>
                        <li>
                            <a href="#"> Sweets</a>
                        </li>
                        <li>
                            <a href="#"> Namkeens</a>
                        </li>
                        <li>
                            <a href="#"> Blogs</a>
                        </li> -->
                    <!-- </ul> -->
                </div>
                <div class="baishnav-navbar-contact-container">
                    <a href="tel: 056596026" class="icon-tel-bs">
                    <div class="nav-telephone-image">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/telephone.svg" alt="Telephone image">
                    </div>
                </a>
                    <a href="tel: 056596026"> <span>Call us</span> :
                        056596026</a>
                </div>
            </div>

        </div>


        <?php 
    }
    ?>

    </header>


    <script>
        theme_directory = "<?php echo get_template_directory_uri() ?>";
    </script>
